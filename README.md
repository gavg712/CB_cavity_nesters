# About repository

The **Cavity nester repository** is a GNU-GPL project. The aim of this repository is provide scripts and data for reproducible research of paper _Global and universal relationships between tree-cavity excavators and forest bird richness_. DOI: [10.1098/rsos.192177](https://doi.org/10.1098/rsos.192177).The repository includes:

  1. `data/cavity_nesters_data.rda`: A table containing richness counts for excavators and other bird groups in each 10×10 km grid cell. These data are derived from maps created by van der Hoek et al. (2017)[^fn1] and Betts et al. (2017)[^fn2]. And `data/bird_groups.rda`, a table of forest bird groups per zooregion.
  2. `output/csv/models` 1000 model statistics per zoogeographic region. Outcomes of gls and alternative models, ran for 1000 iterations. 
  3. `output/csv/tables`. Results Table 1. An example of how our initial results were summarized to constitute Table S2 in the final article.
  4. `output/fig`. Final figures as found in the article.
  5. `R/`Commented scripts for each process and results. Detailed, workable scripts that allow for the reproduction of gls model fitting, tables and plots. 
  6. RStudio project. The complete RStudio environment used to create models and plots.
  
# Session Info

In order to reproduce the results the current session:

``` 
#> R version 3.6.2 (2019-12-12)
#> Platform: x86_64-pc-linux-gnu (64-bit)
#> Running under: Ubuntu 18.04.4 LTS
#> 
#> Matrix products: default
#> BLAS:   /usr/lib/x86_64-linux-gnu/atlas/libblas.so.3.10.3
#> LAPACK: /usr/lib/x86_64-linux-gnu/atlas/liblapack.so.3.10.3
#> 
#> locale:
#>  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=es_EC.UTF-8       
#>  [4] LC_COLLATE=en_US.UTF-8     LC_MONETARY=es_EC.UTF-8    LC_MESSAGES=en_US.UTF-8   
#>  [7] LC_PAPER=es_EC.UTF-8       LC_NAME=C                  LC_ADDRESS=C              
#> [10] LC_TELEPHONE=C             LC_MEASUREMENT=es_EC.UTF-8 LC_IDENTIFICATION=C       
#> 
#> attached base packages:
#> [1] parallel  stats     graphics  grDevices utils     datasets  methods   base     
#> 
#> other attached packages:
#>  [1] nlme_3.1-143     doMC_1.3.6       iterators_1.0.12 foreach_1.4.7    tmap_2.3-1       raster_3.0-7    
#>  [7] sp_1.3-2         forcats_0.4.0    stringr_1.4.0    dplyr_0.8.3      purrr_0.3.3      readr_1.3.1     
#> [13] tidyr_1.0.0      tibble_2.1.3     ggplot2_3.2.1    tidyverse_1.3.0 
#> 
#> loaded via a namespace (and not attached):
#>  [1] fs_1.3.1           sf_0.8-0           lubridate_1.7.4    RColorBrewer_1.1-2 httr_1.4.1        
#>  [6] tools_3.6.2        backports_1.1.5    rgdal_1.4-8        R6_2.4.1           KernSmooth_2.23-16
#> [11] rgeos_0.5-2        DBI_1.1.0          lazyeval_0.2.2     colorspace_1.4-1   withr_2.1.2       
#> [16] tidyselect_0.2.5   processx_3.4.1     leaflet_2.0.3      compiler_3.6.2     cli_2.0.1         
#> [21] rvest_0.3.5        xml2_1.2.2         scales_1.1.0       classInt_0.4-2     callr_3.4.0       
#> [26] digest_0.6.23      rmarkdown_2.0      dichromat_2.0-0    pkgconfig_2.0.3    htmltools_0.4.0   
#> [31] sessioninfo_1.1.1  dbplyr_1.4.2       fastmap_1.0.1      htmlwidgets_1.5.1  rlang_0.4.2       
#> [36] readxl_1.3.1       rstudioapi_0.10    shiny_1.4.0        generics_0.0.2     jsonlite_1.6      
#> [41] crosstalk_1.0.0    magrittr_1.5       Rcpp_1.0.3         munsell_0.5.0      fansi_0.4.1       
#> [46] clipr_0.7.0        lifecycle_0.1.0    stringi_1.4.3      leafsync_0.1.0     whisker_0.4       
#> [51] tmaptools_2.0-2    grid_3.6.2         promises_1.1.0     crayon_1.3.4       lattice_0.20-38   
#> [56] haven_2.2.0        hms_0.5.3          ps_1.3.0           zeallot_0.1.0      knitr_1.26        
#> [61] pillar_1.4.3       codetools_0.2-16   reprex_0.3.0       XML_3.98-1.20      glue_1.3.1        
#> [66] packrat_0.5.0      evaluate_0.14      modelr_0.1.5       vctrs_0.2.1        httpuv_1.5.2      
#> [71] cellranger_1.1.0   gtable_0.3.0       assertthat_0.2.1   xfun_0.11          mime_0.8          
#> [76] lwgeom_0.1-7       xtable_1.8-4       broom_0.5.3        e1071_1.7-3        later_1.0.0       
#> [81] class_7.3-15       viridisLite_0.3.0  units_0.6-5
```

<sup>Created on 2020-01-20 by the [reprex package](https://reprex.tidyverse.org) (v0.3.0)</sup>

# References

[^fn1]: van der Hoek Y, Gaona GV, Martin K. 2017. The diversity, distribution and conservation status of the tree-cavity-nesting birds of the world. Diversity and Distributions 23: 1120–1131. DOI:[10.1111/ddi.12601](https://doi.org/10.1111/ddi.12601).
[^fn2]: Betts MG, Wolf C, Ripple WJ, Phalan B, Millers KA, Duarte A, Butchart SH, Levi T. 2017. Global forest loss disproportionately erodes biodiversity in intact landscapes. Nature 547(7664): 441. DOI:[10.1038/nature23285](https://doi.org/10.1038/nature23285)