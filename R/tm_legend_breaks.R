# Function to modify legend breaks to min and max label
tm_legend_breaks <- function(tmap, n = NULL){
  mxBr <- max(tmap$tm_raster$breaks)
  if(is.null(n)){
    n <- length(tmap$tm_raster$breaks)
  }
  brs <- seq(0, mxBr, by = mxBr/(n-1))
  if(!is.null(n)){
    tmap$tm_raster$breaks <- brs 
  }
  tmap$tm_raster$labels <- c("0", rep("", length(brs)-2), mxBr)
  return(tmap)
}
