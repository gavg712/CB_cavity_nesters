# Files and modelling content:

Each file contain the stats of 1000 GLS models per zoogeographic region adjusted by `nlme::gls()`:

```r
nlme::gls(model = y ~ x, 
          correlation = corExp(form = ~ coord_x) + coord_y),
          data = data_filtered_per_zooregion) 
```

- Correlation 1: in file `Ex-Ne.csv` <---> `non_excavators ~ excavators`
- Correlation 2: in file `Ex-Fs.csv` <---> `forest_specialists ~ excavators`
- Correlation 3: in file `Ex-Fg.csv` <---> `forest_generalists ~ excavators`
- Correlation 4: in file `Ex-Fb.csv` <---> `forest_birds ~ excavators`
- Correlation 1: in file `Wo-Ne.csv` <---> `non_excavators ~ woodpeckers`
- Correlation 2: in file `Wo-Fs.csv` <---> `forest_specialists ~ woodpeckers`
- Correlation 3: in file `Wo-Fg.csv` <---> `forest_generalists ~ woodpeckers`
- Correlation 4: in file `Wo-Fb.csv` <---> `forest_birds ~ woodpeckers`