# Summary tables description:

Summary tables was performed by R script available in  [](https://gitlab.com/gavg712/CB_cavity_nesters/blob/master/R/models_summary_tables.R)

## Data description in Table 1 

Table 1 contain the total number of species and max species per grid cell  (10 x 10 Km) in a zooregion and bird group.
 
 
## Table 2 and Table S1:

Table 2 is the summary for correlations between each _bird group_ vs _Excavators (Ex)_. Table S1 is the summary for correlations between each _bird group_ vs _Woodpeckers (Wo)_. Both tables has the same structure as described bellow:

- `Zoogeographic Region` [col 1]: Name of zoogeographic region used in this paper
- `Correlatio` [col 2]: The model correlation: (`1 = non_excavators ~ [Ex/Wo]`, `2 = forest_specialists ~ [Ex/Wo]`, `3 = forest_generalists ~ [Ex/Wo]`,`4 = forest_birds ~ [Ex/Wo]`)
- `Mean slope (Min. - Max.; SD)` [col 3]: Statistics values as specified by the column name
- `% of models with P < 0.05` [col 4]: Percent of models with predictor pvalue greater than 0.05.
- `Mean Spearman rho` [col 5]: Normal mean of Spearman-Rho coefficient.
